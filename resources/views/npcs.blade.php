@extends("layouts.layout")

@section("content")
  <main class="container">
    <div class="row">
      <div class="col">
        <label for="planet-select">Planet</label>
        <select id="planet-select">
          @foreach( $planets as $planet )
            <option value="{{ $planet->id }}">{{ $planet->name }}</option>
          @endforeach
        </select>
        <label for="map-select">Map</label>
        <select id="map-select">
          @foreach( $maps as $map )
            <option value="{{ $map->id }}">{{ $map->name }}</option>
          @endforeach
        </select>
        <p class="label">Npcs for <span class="zone">{{ $maps[0]-> name}}</span></p>
        <ul id="maps-list" class="small sidebar-list">
          @foreach( $npcs as $npc )
            <li><a href="" data-npc="{{ $npc->id }}" class="npc-link">{{ $npc->name }}</a></li>
          @endforeach
      </div>
      <div class="col-9">
        <canvas id="canvas"></canvas>
        <div id="npc-data"></div>
        <h2>Capabilities</h2>
        <h2>Quests</h2>
      </div>
    </div>
  </main>
@endsection

@section("page_scripts")
<script>
  var xOffset
  var yOffset
$(document).ready(function(){
  window.canvas = document.getElementById("canvas");
  window.context = window.canvas.getContext("2d");
  $.ajaxSetup({
    headers: {
      'X-CSRF-TOKEN': $('meta[name="csrf-token"]').attr('content')
    }
  });
  drawMap({image: "zant.png", canWidth: 384, canHeight: 384, xOffset: 31, yOffset: 30})
  updateCities()
})

$(document).on("click",".npc-link", function(e) {
  e.preventDefault();
  var npc_id = $(this).data("npc")

  updateNpc(npc_id)
})


$("#planet-select").change(function(e) {
  e.preventDefault();
  var planet_id = $("#planet-select option:selected").val()
  updatePlanet(planet_id)
})

$("#map-select").change(function(e) {
  e.preventDefault()
  updateCities()
  getMap($("#map-select option:selected").val())
  $(".zone").text($("#map-select option:selected").text())
})

var updateCities = function() {
  var map_id = $("#map-select option:selected").val()
  var npc_id
  $("#maps-list").html("")
  $(".zone").text($("#map-select option:selected").text())
  $.ajax({
    type: "POST",
    url: "./get-npcs",
    data: {"map_id": map_id},
    dataType: "json",
    success: function(data) {
      var html = ""
      if( data.length > 0 ) {
        npc_id = data[0].id
        for( var i=0; i<data.length; i++ ) {
          html+= "<li><a href='' data-npc='" + data[i].id + "' class='npc-link'> " + data[i].name +"</a>"
        }
        $("#maps-list").html("").html(html)
      } else {
        $("#maps-list").html("<li>This map contains no NPCs</li>")
        $("#npc-data").html("")
      }
      updateNpc(npc_id)
    }
  })
}

var updateNpc = function(npc_id) {
  $.ajax({
    type: "POST",
    url: "./get-npc",
    data: {"npc_id": npc_id},
    dataType: "json",
    success: function(data) {
      drawNPC(data);
      var html = "<p><strong>Name:</strong> " + data[0].name + "<br>" +
                  "<strong>Coordinates:</strong> " + parseInt(data[0].x_index) + ", " + parseInt(data[0].y_index)
      $("#npc-data").html(html)
    }
  })
}

var updatePlanet = function(planet_id) {
  $.ajax({
    type: "POST",
    url: "./get-maps",
    data: {"id": planet_id},
    dataType: "json",
    success: function(data) {
      console.log(data)
      var html = ""
      for( var i=0; i<data.length; i++ ) {
        html+= "<option value='" + data[i].id + "'>" + data[i].name + "</option>"
      }
      $("#map-select").empty().html(html)
      updateCities()
      getMap(data[0].id)
    }
  })
}

var getMap = function(map_id) {
  $.ajax({
    type: "POST",
    url: "./get-map",
    data: {"id": map_id},
    dataType: "json",
    success: function(data) {
console.log(xOffset)
      drawMap(data[0]);
    }
  })
}

var drawMap = function(data) {
  xOffset = data.xOffset
  yOffset = data.yOffset

  window.canvas.width = data.canWidth;
  window.canvas.height = data.canHeight;
  var mapSrc = "img/maps/" + data.image
  $("#canvas").css("background-image", "url(" + mapSrc +")" )
}

var drawNPC = function( npc ) {
  var startOffsetX = ((parseInt(xOffset)) + 0) * 160;
  var startOffsetY = (10400 - ((parseInt(yOffset)) * 160));
  var radius = 5;

  var posX = 64 + ((npc[0].x_index - startOffsetX)/2.5);
  var posY = 64 + ((startOffsetY - npc[0].y_index)/2.5);
  window.context.clearRect(0, 0, canvas.width, canvas.height);
  window.context.beginPath();
  window.context.arc(posX, posY, radius, 0, 2 * Math.PI, false);
  window.context.fillStyle = "#ff0080"
  window.context.fill();
  window.context.lineWidth = 1;
  window.context.strokeStyle = '#003300';
  window.context.stroke(); 
}
</script>

@endsection