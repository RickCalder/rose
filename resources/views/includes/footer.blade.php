<footer>
  <div class="container">
    <div class="row">
      <div class="col">
        &copy;{{date('Y')}} <a href="https://calder.io" target="_blank">calder.io</a>
      </div>
    </div>
  </div>
</footer>