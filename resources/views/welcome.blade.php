@extends("layouts.layout")

@section("content")
  <main class="container">
    <div class="row">
      <div class="col">
        <h1>Rose Online Guide</h1>
        <h2>Events</h2>
        @foreach( $events as $event )
          <h3>{{ $event->title }}</h3>
          {{ date('F d, Y',strtotime($event->start_date)) }} - {{ date('F d, Y',strtotime($event->end_date)) }}
          <div>{!! $event->description !!}</div>
          <hr>
        @endforeach
      </div>
    </div>
  </main>
@endsection

@section("page_scripts")

@endsection