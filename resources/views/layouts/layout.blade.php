<!doctype html>
<html lang="{{ app()->getLocale() }}">
@include("includes.html-head")
<body>
  <div class="wrapper d-flex flex-column">
    @include("includes.header")
    @yield("content")
    @include("includes.footer")
  </div>
  @include("includes.scripts")
  @yield("page_scripts")
</body>
</html>
