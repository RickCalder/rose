-- MySQL dump 10.13  Distrib 5.5.55, for debian-linux-gnu (x86_64)
--
-- Host: localhost    Database: rose
-- ------------------------------------------------------
-- Server version	5.5.55-0ubuntu0.14.04.1

/*!40101 SET @OLD_CHARACTER_SET_CLIENT=@@CHARACTER_SET_CLIENT */;
/*!40101 SET @OLD_CHARACTER_SET_RESULTS=@@CHARACTER_SET_RESULTS */;
/*!40101 SET @OLD_COLLATION_CONNECTION=@@COLLATION_CONNECTION */;
/*!40101 SET NAMES utf8 */;
/*!40103 SET @OLD_TIME_ZONE=@@TIME_ZONE */;
/*!40103 SET TIME_ZONE='+00:00' */;
/*!40014 SET @OLD_UNIQUE_CHECKS=@@UNIQUE_CHECKS, UNIQUE_CHECKS=0 */;
/*!40014 SET @OLD_FOREIGN_KEY_CHECKS=@@FOREIGN_KEY_CHECKS, FOREIGN_KEY_CHECKS=0 */;
/*!40101 SET @OLD_SQL_MODE=@@SQL_MODE, SQL_MODE='NO_AUTO_VALUE_ON_ZERO' */;
/*!40111 SET @OLD_SQL_NOTES=@@SQL_NOTES, SQL_NOTES=0 */;

--
-- Table structure for table `events`
--

DROP TABLE IF EXISTS `events`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `events` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `title` varchar(100) NOT NULL,
  `description` text NOT NULL,
  `start_date` date NOT NULL,
  `end_date` date NOT NULL,
  `created_at` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP ON UPDATE CURRENT_TIMESTAMP,
  `updated_at` timestamp NOT NULL DEFAULT '0000-00-00 00:00:00',
  PRIMARY KEY (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=3 DEFAULT CHARSET=latin1;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `events`
--

LOCK TABLES `events` WRITE;
/*!40000 ALTER TABLE `events` DISABLE KEYS */;
INSERT INTO `events` VALUES (1,'Back to School','<p>The time has come to hit those skill books once again! Join us  in the Canyon City of Zant for some studying, testing, homework, and exam fun! Talk to [Event Info] Judy, to get your classes started: test your knowledge and win some valuable rewards to use in this Back to School Season!</p>\n\n<p>Use your math skills, science prowess, geography knowledge, and even your P.E. stamina to earn some cool Off-hand school supplies such as Pencils, Crayons, Rulers, and much more!</p>\n\n<p>Special Chemistry Boxes will be dropping in all zones during the event. For those of you who have completed your Final Exams, Judy is looking for all the school spirit she can find. If you can get through all the studies and collect enough School Spirit, she will offer one of two special permanent mounts, the Krawfy or Green Krawfy Mount!</p>\n\n<p>Don\'t be left out, the Back to School Event starts August 20, and end September 8!</p>\n\n<p>Join us for some fantastic academic fun!</p>','2017-08-20','2017-09-08','2017-08-27 11:06:25','0000-00-00 00:00:00'),(2,'Magic is Everywhere','<p>Magic has returned again to ROSE Online! You can feel it in the air, the water, and every nook and cranny throughout the worlds right? Our resident ROSE creatures have taken this season’s opportunity to relocate their horded stashes filled with magic and friendship from their hiding places; this is your chance to catch them and share in their stash! Defeat monsters throughout Junon, Luna, Eldeon and Orlo and you might have a chance to get your hands on one of these mysterious boxes!</p>\r\n\r\n<p>Open the boxes to discover rare items once worn by friendly ancient warriors. Every box offers a slim chance to get one of those ancient and rare pieces of equipment! Use a Gold Key (In the Item Mall: Specials > Special) to guarantee your choice of a costume item (head or tail). Using a Silver Key will guarantee one random costume piece!</p>\r\n\r\n<p>Didn’t get the one costume piece you desire? Bring your costume piece to [Designer] Keenu in the Canyon City of Zant! She\'s been collecting all the pristine beautiful pieces she can get her hands on. If you speak with her she will even exchange your unwanted piece for a coupon. Every 3 coupons will grant you one exchange with a costume piece from her collection! Using 5 coupons will allow you to choose between a headpiece or back item!</p>\r\n\r\n<p>Magic is everywhere this summer so let\'s end it by collecting yourself some fantastic rare items! This adventurer lasts until the end of August!</p>\r\n<p>Don’t miss out on the magic!</p>','2017-08-01','2017-08-31','2017-08-27 11:08:58','0000-00-00 00:00:00');
/*!40000 ALTER TABLE `events` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `maps`
--

DROP TABLE IF EXISTS `maps`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `maps` (
  `id` int(11) NOT NULL,
  `name` varchar(125) NOT NULL,
  `planet_id` int(11) NOT NULL,
  `image` varchar(100) NOT NULL,
  `canWidth` int(11) NOT NULL DEFAULT '384',
  `canHeight` int(11) NOT NULL DEFAULT '384',
  `xOffset` int(11) NOT NULL DEFAULT '31',
  `yOffset` int(11) NOT NULL DEFAULT '30',
  `created_at` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP,
  `updated_at` timestamp NULL DEFAULT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB DEFAULT CHARSET=latin1;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `maps`
--

LOCK TABLES `maps` WRITE;
/*!40000 ALTER TABLE `maps` DISABLE KEYS */;
INSERT INTO `maps` VALUES (1,'Zant',1,'zant.png',384,384,31,30,'0000-00-00 00:00:00',NULL),(2,'City of Junon Polis',1,'junon_polis.png',576,512,30,30,'0000-00-00 00:00:00',NULL),(3,'Dolphin Island',1,'dolphin_island.png',512,448,31,30,'0000-00-00 00:00:00',NULL),(21,'Valley of Luxem Tower',1,'luxem_tower.png',384,384,31,30,'0000-00-00 00:00:00',NULL),(22,'Adventurer\'s Plains',1,'adventurer_plains.png',519,384,31,30,'0000-00-00 00:00:00',NULL),(23,'Breezy Hills',1,'breezy_hills.png',384,448,31,30,'0000-00-00 00:00:00',NULL),(24,'El Verloon Desert',1,'el_verloon.png',448,448,31,30,'0000-00-00 00:00:00',NULL),(25,'Anima Lake',1,'anima_lake.png',448,448,31,30,'0000-00-00 00:00:00',NULL),(26,'Forest of Wisdom',1,'forest_of_wisdom.png',448,448,31,30,'0000-00-00 00:00:00',NULL),(27,'Kenji Beach',1,'kenji_beach.png',512,384,31,30,'0000-00-00 00:00:00',NULL),(28,'Gorge of Silence',1,'gorge_of_silence.png',512,512,31,30,'0000-00-00 00:00:00',NULL),(29,'Desert of the Dead',1,'desert_of_the_dead.png',512,384,31,31,'0000-00-00 00:00:00',NULL),(31,'Goblin Caves B1',1,'goblin_caves_b1.png',384,384,31,30,'0000-00-00 00:00:00',NULL),(32,'Goblin Caves B2',1,'goblin_caves_b2.png',384,384,31,30,'0000-00-00 00:00:00',NULL),(33,'Goblin Caves B3',1,'goblin_caves_b3.png',448,448,31,30,'0000-00-00 00:00:00',NULL),(36,'Sunshine Coast',1,'sunshine1.png',384,384,31,31,'0000-00-00 00:00:00',NULL),(37,'Sunshine Coast',1,'sunshine2.png',384,384,31,31,'0000-00-00 00:00:00',NULL),(38,'Santa Planetoid',1,'santa.png',384,384,31,31,'0000-00-00 00:00:00',NULL),(40,'Grand Ballroom',1,'grand_ballroom.png',384,384,32,32,'0000-00-00 00:00:00',NULL),(41,'Oblivion Temple',1,'oblivion_temple.png',256,256,31,31,'0000-00-00 00:00:00',NULL),(42,'Oblivion Temple B2',1,'oblivion_temple_b2.png',448,384,31,31,'0000-00-00 00:00:00',NULL),(43,'Oblivion Temple B3',1,'oblivion_temple_b3.png',386,386,31,31,'0000-00-00 00:00:00',NULL),(44,'Oblivion Temple B4',1,'oblivion_temple_b4.png',388,388,31,31,'0000-00-00 00:00:00',NULL),(51,'Magic City of the Eucar',2,'eucar.png',448,448,31,31,'0000-00-00 00:00:00',NULL),(52,'Mana Snowfields',2,'mana_snowfields.png',640,576,31,31,'0000-00-00 00:00:00',NULL),(53,'Arumic Valley',2,'arumic.png',704,448,31,31,'0000-00-00 00:00:00',NULL),(54,'Crystal Snowfields',2,'crystal.png',512,768,31,30,'0000-00-00 00:00:00',NULL),(55,'Freezing Plateau',2,'freezing.png',512,576,31,31,'0000-00-00 00:00:00',NULL),(56,'Forgotten Temple B1',2,'forgotten1.png',448,704,31,31,'0000-00-00 00:00:00',NULL),(57,'Forgotten Temple B2',2,'forgotten2.png',512,384,31,31,'0000-00-00 00:00:00',NULL),(58,'Mount Eruca',2,'eruca.png',642,642,31,31,'0000-00-00 00:00:00',NULL),(59,'Luna Clan Field',2,'luna_clan_field.png',384,384,31,31,'0000-00-00 00:00:00',NULL),(61,'Xita Refuge',3,'xita.png',512,576,31,31,'2017-08-20 16:02:37',NULL),(62,'Shady Jungle',3,'shady.png',832,576,31,31,'2017-08-20 16:02:37',NULL),(63,'Forest of Wandering',3,'forest_wandering.png',639,704,31,31,'2017-08-20 16:03:15',NULL),(64,'Marsh of Ghosts',3,'mog.png',448,384,31,31,'2017-08-20 16:03:15',NULL),(65,'Sikuku Underground Prison',3,'sikuku_prison.png',447,382,31,31,'2017-08-20 16:04:02',NULL),(66,'Sikuku Ruins',3,'sikuku_ruins.png',640,768,31,31,'2017-08-20 16:04:02',NULL),(71,'Desert City of Muris',4,'muris.png',320,576,31,30,'2017-08-20 16:04:53',NULL),(72,'Portal Room',4,'portal_room.png',256,256,31,31,'2017-08-20 16:04:53',NULL),(73,'Orlean Portal Temple',4,'orlean.png',384,322,31,31,'2017-08-20 16:05:34',NULL),(78,'The Wasteland',4,'wasteland1.png',448,320,31,31,'2017-08-20 16:05:34',NULL),(79,'The Wasteland',4,'wasteland2.png',448,320,31,31,'2017-08-20 16:06:11',NULL),(80,'Ancient Oasis Shrine',4,'oasis_shrine.png',256,256,31,31,'2017-08-20 16:06:11',NULL),(81,'Wasteland Ruins Path',4,'wasteland_ruins.png',448,384,30,31,'2017-08-20 16:06:47',NULL),(83,'The Golden Ring',4,'golden_ring.png',448,512,31,31,'2017-08-20 16:06:47',NULL),(85,'Fossil Sanctuary',4,'fossil.png',448,512,31,31,'2017-08-20 16:07:15',NULL);
/*!40000 ALTER TABLE `maps` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `migrations`
--

DROP TABLE IF EXISTS `migrations`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `migrations` (
  `id` int(10) unsigned NOT NULL AUTO_INCREMENT,
  `migration` varchar(255) COLLATE utf8mb4_unicode_ci NOT NULL,
  `batch` int(11) NOT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `migrations`
--

LOCK TABLES `migrations` WRITE;
/*!40000 ALTER TABLE `migrations` DISABLE KEYS */;
/*!40000 ALTER TABLE `migrations` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `npc_abilities`
--

DROP TABLE IF EXISTS `npc_abilities`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `npc_abilities` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `ability` varchar(100) NOT NULL,
  `ability_description` text NOT NULL,
  `created_at` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP,
  `updated_at` timestamp NULL DEFAULT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=10 DEFAULT CHARSET=latin1;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `npc_abilities`
--

LOCK TABLES `npc_abilities` WRITE;
/*!40000 ALTER TABLE `npc_abilities` DISABLE KEYS */;
INSERT INTO `npc_abilities` VALUES (1,'Armour Seller','This NPC sells armour.','2017-08-19 15:49:49',NULL),(2,'Weapon Seller','This NPC sells weapons.','2017-08-19 15:49:49',NULL),(3,'Jewelry Seller','This NPC sells Jewelry','2017-08-19 15:50:18',NULL),(4,'Food Seller','This NPC sells food','2017-08-19 15:50:18',NULL),(5,'Scroll Seller','This NPC sells teleportation scrolls','2017-08-19 15:50:55',NULL),(6,'Magic items seller','This NPC sells magical items','2017-08-19 15:50:55',NULL),(7,'Item appraisal','This NPC performs item appraisal. Some armour and weapons when dropped by mobs have the text \"Item appraisal required\". This means that after appraisal the item will have a set of random sub-stats. This NPC can unveil those sub-stats.','2017-08-19 15:52:42',NULL),(8,'Item repair','This NPC can repair damaged weapons and armour without any loss of durability.','2017-08-19 15:52:42',NULL),(9,'Refining','This NPC can refine armour and weapons.','2017-08-19 15:53:22',NULL);
/*!40000 ALTER TABLE `npc_abilities` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `npcs`
--

DROP TABLE IF EXISTS `npcs`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `npcs` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `name` varchar(255) NOT NULL,
  `npc_id` int(11) NOT NULL,
  `map_id` int(11) NOT NULL,
  `z_index` float NOT NULL,
  `x_index` float NOT NULL,
  `y_index` float NOT NULL,
  `display` int(11) NOT NULL DEFAULT '1',
  `created_at` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP,
  `updated_at` timestamp NULL DEFAULT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=313 DEFAULT CHARSET=latin1;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `npcs`
--

LOCK TABLES `npcs` WRITE;
/*!40000 ALTER TABLE `npcs` DISABLE KEYS */;
INSERT INTO `npcs` VALUES (1,'[Livestock Farmer] Lampa',1016,1,173.934,5109.42,5363.68,1,'0000-00-00 00:00:00',NULL),(2,'[Village Chief] Cornell',1001,1,12.55,5096.51,5256.04,1,'0000-00-00 00:00:00',NULL),(3,'[Junon Order Co-Founder] Francis',1003,1,157.957,5080.3,5066.28,1,'0000-00-00 00:00:00',NULL),(4,'[NoNameNPC]',1753,1,181.501,5233.74,5307.19,0,'0000-00-00 00:00:00',NULL),(5,'[Righteous Crusader] Leonard',1005,1,266.005,5127.04,5208.73,1,'0000-00-00 00:00:00',NULL),(6,'[Resident] Luth',1015,1,270.003,5130.61,5154.58,1,'0000-00-00 00:00:00',NULL),(7,'[Event Info] Judy',1201,1,162.011,5261.88,5212.97,1,'0000-00-00 00:00:00',NULL),(8,'[Monster Coin Trader] Dahved',1492,1,0,5245.1,5164.56,1,'0000-00-00 00:00:00',NULL),(9,'[Eccentric Inventor] Spero',1011,1,0,5273.1,5267.12,1,'0000-00-00 00:00:00',NULL),(10,'[Weapon Seller] Raffle',1008,1,3.008,5213.73,5260.83,1,'0000-00-00 00:00:00',NULL),(11,'[Armor Seller] Carrion',1009,1,34.535,5198.3,5253.83,1,'0000-00-00 00:00:00',NULL),(12,'[Guide] Lena',1014,1,333.435,5272.68,5079.04,1,'0000-00-00 00:00:00',NULL),(13,'[Ferrell Guild Staff] Crow',1004,1,180.5,5210.44,5104.79,1,'0000-00-00 00:00:00',NULL),(14,'[Akram Kingdom Minister] Warren',1002,1,181.501,5132.56,5078.95,1,'0000-00-00 00:00:00',NULL),(15,'[NoNameNPC]',1751,1,0,5266.17,5072.05,0,'0000-00-00 00:00:00',NULL),(16,'[Cornell\'s Grandson] Cheney',1018,1,22.043,5435.3,5492.26,1,'0000-00-00 00:00:00',NULL),(17,'Woopie King',3019,1,322.406,5420.39,5316.2,0,'0000-00-00 00:00:00',NULL),(18,'[Designer] Keenu',1010,1,238.556,5319.51,5151.29,1,'0000-00-00 00:00:00',NULL),(19,'[Ferrell Guild Staff] Ulysses',1012,1,333.435,5291.1,5264.22,1,'0000-00-00 00:00:00',NULL),(20,'[Gypsy Merchant] Mina',1007,1,307.514,5308.01,5241.04,1,'0000-00-00 00:00:00',NULL),(21,'[Arumic Merchant] Tryteh',1006,1,298.187,5316.27,5212.11,1,'0000-00-00 00:00:00',NULL),(22,'[NoNameNPC]',1752,1,93.995,5356.19,5187.52,0,'0000-00-00 00:00:00',NULL),(23,'[Keenu\'s Daughter] Arisa',1020,1,275.985,5428.17,5082.68,1,'0000-00-00 00:00:00',NULL),(24,'[Tavern Owner] Sharlin',1013,1,191.421,5300.45,5112.64,1,'0000-00-00 00:00:00',NULL),(25,'[Lena\'s Sister] Shirley',1019,1,322.406,5504.69,5274.08,1,'0000-00-00 00:00:00',NULL),(26,'Christmas Event Spawn Control NPC1',3040,2,0,4830,4645,0,'0000-00-00 00:00:00',NULL),(27,'Christmas Event Spawn Control NPC2',3041,2,0,4833,4645,0,'0000-00-00 00:00:00',NULL),(28,'Spawn Control NPC 3',3042,2,0,4835,4645,0,'0000-00-00 00:00:00',NULL),(29,'Pirate Event Spawn Control (NPC)',3044,2,0,4840,4645,0,'0000-00-00 00:00:00',NULL),(30,'[Battle Merchant] Rena',2004,2,119.834,5278.02,5225.14,1,'0000-00-00 00:00:00',NULL),(31,'[Rena\'s Summon] Third Eye',2005,2,77.589,5277.8,5227.7,1,'0000-00-00 00:00:00',NULL),(32,'[Righteous Crusader] Gawain',1090,2,84.016,5334.58,5353.91,1,'0000-00-00 00:00:00',NULL),(33,'[Righteous Crusader] Huffe',1111,2,90,5334.84,5345.81,1,'0000-00-00 00:00:00',NULL),(34,'[Righteous Crusader] Norg',3023,2,88.001,5326.17,5349.93,1,'0000-00-00 00:00:00',NULL),(35,'[Clan Manager] Burtland',1115,2,19.295,5290.6,5267.6,1,'0000-00-00 00:00:00',NULL),(36,'[Honor Items] Henry',2001,2,134.288,5290.83,5235.59,1,'0000-00-00 00:00:00',NULL),(37,'[Honor Weapons] William',2002,2,277.961,5345.67,5265.91,1,'0000-00-00 00:00:00',NULL),(38,'[Honor Armors] Theodor',2003,2,254.3,5345.55,5230.2,1,'0000-00-00 00:00:00',NULL),(39,'[Battlemaster] Esmee',1990,2,88.001,5285.04,5252.55,1,'0000-00-00 00:00:00',NULL),(40,'[Knight of Junon Order] Sammodell',3021,2,271.999,5329.68,5136.27,1,'0000-00-00 00:00:00',NULL),(41,'[Founder of Junon Order] Raw',1088,2,181.501,5319.27,5095.36,1,'0000-00-00 00:00:00',NULL),(42,'[Junon Order Elder] Gorthein',1109,2,181.501,5325.54,5095.31,1,'0000-00-00 00:00:00',NULL),(43,'[Valor Materials] Mason',2011,2,179.5,5346.26,5113.35,1,'0000-00-00 00:00:00',NULL),(44,'[Valor Weapons] Jason',2012,2,180,5306.26,5112.41,1,'0000-00-00 00:00:00',NULL),(45,'[Valor Armors] Carson',2013,2,180,5297.27,5112.72,1,'0000-00-00 00:00:00',NULL),(46,'[Valor Skills] Greyson',2010,2,180,5337.71,5113.76,1,'0000-00-00 00:00:00',NULL),(47,'[Guide] Eva',1082,2,153.435,5505.78,5346.09,1,'0000-00-00 00:00:00',NULL),(48,'[Soldier] Odelo',1116,2,0,5507.31,5397.4,1,'0000-00-00 00:00:00',NULL),(49,'[Soldier] Winters',1117,2,0,5522.99,5397.75,1,'0000-00-00 00:00:00',NULL),(50,'[NoNameNPC]',1755,2,0,5515,5389.7,0,'0000-00-00 00:00:00',NULL),(51,'[NoNameNPC]',1756,2,180,5515.67,5418.25,0,'0000-00-00 00:00:00',NULL),(52,'[Mayor] Darren',1081,2,178.499,5514.48,5238.2,1,'0000-00-00 00:00:00',NULL),(53,'[Event Info] Felice Fete',1202,2,206.565,5562,5164.83,1,'0000-00-00 00:00:00',NULL),(54,'[Monster Coin Trader] Vedad',1493,2,335.763,5559.95,5254.14,1,'0000-00-00 00:00:00',NULL),(55,'[Akram Ambassador] Adalric',1108,2,155.002,5463.48,5168.3,1,'0000-00-00 00:00:00',NULL),(56,'[Interplanetary Guide] Alphonso',1118,2,178.499,5520.56,5063.66,1,'0000-00-00 00:00:00',NULL),(57,'[Historian] Jones',1104,2,162.011,5578.19,4890.44,1,'0000-00-00 00:00:00',NULL),(58,'[Vicious Captain] Ruven',1125,2,270,5597.76,4918.87,1,'0000-00-00 00:00:00',NULL),(59,'[Manager of Ferrell] Arothel',1089,2,270,5713.43,5360.16,1,'0000-00-00 00:00:00',NULL),(60,'[Armor Merchant] Saki',1094,2,258.129,5721.8,5290.79,1,'0000-00-00 00:00:00',NULL),(61,'[Ferrell Guild Staff] Charrs',1110,2,270,5713.38,5368.71,1,'0000-00-00 00:00:00',NULL),(62,'[Ferrell Bank Guard] Arnold',3024,2,271.999,5719.55,5364.48,1,'0000-00-00 00:00:00',NULL),(63,'[NoNameNPC]',1754,2,29.044,5677.23,5342.73,0,'0000-00-00 00:00:00',NULL),(64,'[Gypsy Merchant] Bellia',1092,2,217.594,5725.11,5199.04,1,'0000-00-00 00:00:00',NULL),(65,'[Weapon Merchant] Crune',1093,2,335.763,5728.67,5224.89,1,'0000-00-00 00:00:00',NULL),(66,'[Designer] Lisa',1095,2,220.893,5729.06,5136.33,1,'0000-00-00 00:00:00',NULL),(67,'[Ferrell Guild Merchant] Mildun',1096,2,165.707,5673.16,5193.89,1,'0000-00-00 00:00:00',NULL),(68,'[Tavern Owner] Harin',1097,2,90,5681.79,5131.11,1,'0000-00-00 00:00:00',NULL),(69,'[Premium Shop] Lyon',1080,2,165.112,5649,5185.78,1,'0000-00-00 00:00:00',NULL),(70,'[Premium Exchanger] Anthonios',1113,2,195.491,5654.67,5185.73,1,'0000-00-00 00:00:00',NULL),(71,'[Arumic Merchant] Chelsie',1091,2,211.693,5723.96,5087.24,1,'0000-00-00 00:00:00',NULL),(72,'[Ferrell Guild Staff] Kiroth',1098,2,275.984,5603.58,5058.1,1,'0000-00-00 00:00:00',NULL),(73,'[Ferrell Guild Staff] Hayen',1099,2,258.129,5603.77,5046.12,1,'0000-00-00 00:00:00',NULL),(74,'[Ferrell Guild Staff] Itz',1100,2,270,5603.69,5034.52,1,'0000-00-00 00:00:00',NULL),(75,'[Arumic Researcher] Carasia',1112,2,197.989,5730.57,5094.06,1,'0000-00-00 00:00:00',NULL),(76,'[Arumic Mercenary] Turenth',3022,2,204.237,5730.12,5088.24,1,'0000-00-00 00:00:00',NULL),(77,'[Captain] Peg Leg',3020,2,160.035,5670.86,4910.2,1,'0000-00-00 00:00:00',NULL),(78,'[Livestock Farmer] Sicru',1107,2,187.631,5818.02,5487.59,1,'0000-00-00 00:00:00',NULL),(79,'[Ferrell Guild Staff] Rooen',1513,3,0,5104.48,5019.71,1,'0000-00-00 00:00:00',NULL),(80,'Spawn Control NPC 4',3043,4,0,5263.42,5430.04,1,'0000-00-00 00:00:00',NULL),(81,'[Referee] Carlon',1149,5,194.607,5205.06,5490.5,1,'0000-00-00 00:00:00',NULL),(82,'[Referee] Rodath',1086,5,271.999,5248.14,5279.2,1,'0000-00-00 00:00:00',NULL),(83,'[Referee] Windall',1087,5,341.44,5205.4,5069.9,1,'0000-00-00 00:00:00',NULL),(84,'[Akram Minister] Gamp',1084,6,271.999,5248.14,5279.2,1,'0000-00-00 00:00:00',NULL),(85,'[Visitor Guide] Arua\'s Fairy',1784,9,0,5026.43,5144.66,0,'0000-00-00 00:00:00',NULL),(86,'[Visitor Guide] Arua\'s Fairy',1783,9,68.212,5132.13,4725.68,0,'0000-00-00 00:00:00',NULL),(87,'[Visitor Guide] Arua\'s Fairy',1780,9,16.188,5336.54,5388.02,0,'0000-00-00 00:00:00',NULL),(88,'[Visitor Guide] Arua\'s Fairy',1781,9,341.932,5698.81,5179.7,0,'0000-00-00 00:00:00',NULL),(89,'[Visitor Guide] Arua\'s Fairy',1782,9,180,5656.63,4751.84,0,'0000-00-00 00:00:00',NULL),(90,'[Akram Minister] Nell',1085,11,84.016,4953.37,4992.35,1,'0000-00-00 00:00:00',NULL),(91,'[Akram Minister] Nell',1085,11,10.865,4978.48,5478.6,1,'0000-00-00 00:00:00',NULL),(92,'[Akram Minister] Nell',1085,11,337.957,5455.57,5478.99,1,'0000-00-00 00:00:00',NULL),(93,'[Akram Minister] Nell',1085,11,335.763,5521.94,5020.66,1,'0000-00-00 00:00:00',NULL),(94,'[Akram Minister] Nell',1085,12,84.016,4953.37,4992.35,1,'0000-00-00 00:00:00',NULL),(95,'[Akram Minister] Nell',1085,12,10.865,4978.48,5478.6,1,'0000-00-00 00:00:00',NULL),(96,'[Akram Minister] Nell',1085,12,337.957,5455.57,5478.99,1,'0000-00-00 00:00:00',NULL),(97,'[Akram Minister] Nell',1085,12,335.763,5521.94,5020.66,1,'0000-00-00 00:00:00',NULL),(98,'[Akram Minister] Nell',1085,13,84.016,4953.37,4992.35,1,'0000-00-00 00:00:00',NULL),(99,'[Akram Minister] Nell',1085,13,10.865,4978.48,5478.6,1,'0000-00-00 00:00:00',NULL),(100,'[Akram Minister] Nell',1085,13,337.957,5455.57,5478.99,1,'0000-00-00 00:00:00',NULL),(101,'[Akram Minister] Nell',1085,13,335.763,5521.94,5020.66,1,'0000-00-00 00:00:00',NULL),(102,'[Arumic Researcher] Lutis',1051,21,160.035,5118.14,5374.91,1,'0000-00-00 00:00:00',NULL),(103,'[Cleric] Karitte',1053,21,95.984,5112.44,5385.08,1,'0000-00-00 00:00:00',NULL),(104,'[Stranded Resident] Morgan',1132,21,0,5354.81,5014.18,1,'0000-00-00 00:00:00',NULL),(105,'[Fruit Seller] Magdalena',1135,21,353.933,5513.39,5248.06,1,'0000-00-00 00:00:00',NULL),(106,'[Resident] Rudolfo',1134,21,173.415,5501.85,5211.11,1,'0000-00-00 00:00:00',NULL),(107,'[Livestock Farmer] Enu',1133,21,24.237,5496.56,5248,1,'0000-00-00 00:00:00',NULL),(108,'[Smith] Rockwell',1138,21,105.7,5441.02,5191.52,1,'0000-00-00 00:00:00',NULL),(109,'[Weapon Merchant] Dune',1139,21,165.112,5458.63,5181.31,1,'0000-00-00 00:00:00',NULL),(110,'[Mountain Guide] Shannon',1052,21,0,5472.73,5070.27,1,'0000-00-00 00:00:00',NULL),(111,'Melendino',1473,22,212.618,5095.21,5312.66,1,'0000-00-00 00:00:00',NULL),(112,'[Muse Trainer] Media',1057,22,173.933,5099.13,5405.39,1,'0000-00-00 00:00:00',NULL),(113,'[Hawker Trainer] Connor',1058,22,328.307,5110.77,5432.79,1,'0000-00-00 00:00:00',NULL),(114,'[Dealer Trainer] Titania',1059,22,0,5094.77,5437.11,1,'0000-00-00 00:00:00',NULL),(115,'[Soldier Trainer] Murphy',1056,22,40.893,5054.37,5439.15,1,'0000-00-00 00:00:00',NULL),(116,'[Pumpkin Farmer] Claude',1060,22,95.984,5126.59,5280.7,1,'0000-00-00 00:00:00',NULL),(117,'[Mysterious Traveler] Edeline',1200,22,330.956,5158.66,5304.47,1,'0000-00-00 00:00:00',NULL),(118,'[Boy by the Cart]Huey',1021,22,323.451,5355.05,5401.97,1,'0000-00-00 00:00:00',NULL),(119,'[Path Guard] Bert',1065,22,19.965,5457.67,5387.48,1,'0000-00-00 00:00:00',NULL),(120,'[Smith] Ronk',1034,22,176.992,5653.93,5152.59,1,'0000-00-00 00:00:00',NULL),(121,'[Little Street Vendor] Pony',1035,22,145.465,5637.8,5157.9,1,'0000-00-00 00:00:00',NULL),(122,'[Village Chief] Gray',1038,22,90,5627.19,5172.31,1,'0000-00-00 00:00:00',NULL),(123,'[Ferrell Guild Staff] Seyon',1036,22,23.492,5632.37,5194.24,1,'0000-00-00 00:00:00',NULL),(124,'[Old Man] Myad',1041,22,270,5669.22,5180.15,1,'0000-00-00 00:00:00',NULL),(125,'[Akram Minister] Mairard',1032,22,337.957,5662.79,5196.41,1,'0000-00-00 00:00:00',NULL),(126,'[Messenger of Love] Qpid',1217,22,343.898,5654.65,5188.68,1,'0000-00-00 00:00:00',NULL),(127,'[Visitor Guide] Arua\'s Fairy',1030,22,123.017,5699.97,5119.75,1,'0000-00-00 00:00:00',NULL),(128,'[Ferrell Guild Staff] Peron',1511,22,352.369,5828.29,5205.66,1,'0000-00-00 00:00:00',NULL),(129,'[Novice Designer] Cassirin',1040,22,340.035,5896.42,5148.75,1,'0000-00-00 00:00:00',NULL),(130,'[Breezy Hills Militia] Mercucio',1067,23,311.684,5080.09,5194.14,1,'0000-00-00 00:00:00',NULL),(131,'[Ferrell Guild Staff] Belz',1061,23,270,5358.63,5058.99,1,'0000-00-00 00:00:00',NULL),(132,'[Smith] Punwell',1062,23,24.237,5340.65,5070.26,1,'0000-00-00 00:00:00',NULL),(133,'[Little Street Vendor] Mile',1063,23,307.514,5356.41,5043.28,1,'0000-00-00 00:00:00',NULL),(134,'[Weapon Craftsman] Mairath',1064,23,180,5349.02,5033.95,1,'0000-00-00 00:00:00',NULL),(135,'[Breezy Hills Militia] Rahl',1070,23,24.237,5472.98,5516.27,1,'0000-00-00 00:00:00',NULL),(136,'[El Verloon Tour Guide] Franklin',1140,24,159.355,5024.51,5092.78,1,'0000-00-00 00:00:00',NULL),(137,'[Tourist] Linda',1145,24,0,5120.16,5500.81,1,'0000-00-00 00:00:00',NULL),(138,'[El Verloon Tourism Board] Caraan',1074,24,0,5383.02,4918.3,1,'0000-00-00 00:00:00',NULL),(139,'[Tourist] Rick',1146,24,200.645,5531.21,5370.05,1,'0000-00-00 00:00:00',NULL),(140,'[Archaeologist] Williams',1075,24,0,5473.2,5137.21,1,'0000-00-00 00:00:00',NULL),(141,'[Ferrell Guild Merchant] Lina',1071,24,293.031,5567.26,4927.03,1,'0000-00-00 00:00:00',NULL),(142,'[Gypsy Merchant] Methio',1072,24,192.55,5563.13,4895.45,1,'0000-00-00 00:00:00',NULL),(143,'[Ikaness Staff] Orias',1073,24,95.984,5515.26,4925.14,1,'0000-00-00 00:00:00',NULL),(144,'[Fruit Cart] Feuey',1114,24,225.712,5564.61,4955.06,1,'0000-00-00 00:00:00',NULL),(145,'[Mountain Guide] Stephano',1147,25,303.017,5037.56,5466.84,1,'0000-00-00 00:00:00',NULL),(146,'[Frightened Guard] Canterbury',1148,25,193.124,5087.44,5249.22,1,'0000-00-00 00:00:00',NULL),(147,'[Ikaness Staff] Shroon',1121,25,9.228,5375.78,5188.33,1,'0000-00-00 00:00:00',NULL),(148,'[Ghost] Harry',1790,25,350.772,5506.81,5555.03,1,'0000-00-00 00:00:00',NULL),(149,'[Mountain Guide] Pein',1122,25,307.514,5503.22,5350.93,1,'0000-00-00 00:00:00',NULL),(150,'[Gypsy Merchant] Edone',1123,25,187.631,5448.14,4976.38,1,'0000-00-00 00:00:00',NULL),(151,'[NoNameNPC]',1750,25,322.406,5714.62,5405.4,0,'0000-00-00 00:00:00',NULL),(152,'[Mountain Guide] Kay',1131,26,22.043,5669.16,5105.24,1,'0000-00-00 00:00:00',NULL),(153,'[Ferrell Guild Merchant ] Bith',1143,27,197.989,5259.72,5147.64,1,'0000-00-00 00:00:00',NULL),(154,'[Ferrell Guild Staff] Robin',1512,27,264.016,5239.23,5040.74,1,'0000-00-00 00:00:00',NULL),(155,'[Resident] Hotch',1144,27,189.228,5384.23,5180.1,1,'0000-00-00 00:00:00',NULL),(156,'[Righteous Crusader] Gallahad',1141,27,178.499,5345.2,5047.71,1,'0000-00-00 00:00:00',NULL),(157,'[Akram Minister] Luce',1142,27,101.871,5320.52,5081.27,1,'0000-00-00 00:00:00',NULL),(158,'[Gypsy Merchant] Tobar',1130,28,118.187,5587.22,4732.54,1,'0000-00-00 00:00:00',NULL),(159,'[Shop Waitress] Betty',1127,28,272,5688.74,4802.46,1,'0000-00-00 00:00:00',NULL),(160,'[Storage Keeper] Marcus',1128,28,34.535,5625.19,4853.54,1,'0000-00-00 00:00:00',NULL),(161,'[Ferrell Guild Merchant] Med',1151,28,345.707,5686.58,4902.11,1,'0000-00-00 00:00:00',NULL),(162,'[Blacksmith] Brock',1129,28,182.002,5667.78,4701.27,1,'0000-00-00 00:00:00',NULL),(163,'[Dead Priest] Lantore',1154,29,241.813,5114.61,5131.16,1,'0000-00-00 00:00:00',NULL),(164,'[Town Girl] Lithia',1156,29,24.237,5091.98,5181.81,1,'0000-00-00 00:00:00',NULL),(165,'[Ghost] Harry',1157,29,107.577,5050.08,5123.27,1,'0000-00-00 00:00:00',NULL),(166,'[Ranger] Paul',1155,29,330.956,5157.1,5176.39,1,'0000-00-00 00:00:00',NULL),(167,'[Beach Resident] Judith',1204,36,358.499,5115.68,5324.15,1,'0000-00-00 00:00:00',NULL),(168,'[Beach Resident] Judith',1216,37,358.499,5115.68,5324.15,1,'0000-00-00 00:00:00',NULL),(169,'[Pick-A-Card] Alejandro',1045,37,347.45,5220.72,5357.38,1,'0000-00-00 00:00:00',NULL),(170,'[Clown Throw] Mildred',1044,37,0,5246.65,5366.65,1,'0000-00-00 00:00:00',NULL),(171,'[Junon RPS] Sherisse',1043,37,0,5259.34,5367.26,1,'0000-00-00 00:00:00',NULL),(172,'[Digging for Treasure] Augustus',1048,37,293.031,5227.87,5324.44,1,'0000-00-00 00:00:00',NULL),(173,'[Festival Manager] Lero',1050,37,31.693,5182.5,5340.08,1,'0000-00-00 00:00:00',NULL),(174,'[Beach Resident] Casey Jean',1210,37,16.102,5256.56,5304.59,1,'0000-00-00 00:00:00',NULL),(175,'[Scavenger Hunt] Pirelli',1049,37,198.637,5334.27,5289.01,1,'0000-00-00 00:00:00',NULL),(176,'[Chicken Races] Hig',1047,37,347.45,5297.48,5362.05,1,'0000-00-00 00:00:00',NULL),(177,'[Pie Crush] Tony',1046,37,343.898,5325.37,5365.62,1,'0000-00-00 00:00:00',NULL),(178,'[Old Fisherman] Sal',1066,37,197.351,5298.5,5045.7,1,'0000-00-00 00:00:00',NULL),(179,'[Event Info] Santa Claus',1205,38,3.012,5069.18,5362.51,1,'0000-00-00 00:00:00',NULL),(180,'[Event Info] Loelsch',1206,38,66.941,5057.19,5351.61,1,'0000-00-00 00:00:00',NULL),(181,'[Evil Reindeer] Bob',1490,38,97.961,5065.61,5206.88,1,'0000-00-00 00:00:00',NULL),(182,'[Event Info] Lilly',1164,39,30.79,5054.89,5347.89,1,'0000-00-00 00:00:00',NULL),(183,'[Festival Manager] Lero',1055,39,0,5072.25,5363.08,1,'0000-00-00 00:00:00',NULL),(184,'[Cherry Berry Pie Crush] Anya',1175,39,315.541,5061.64,5094.96,1,'0000-00-00 00:00:00',NULL),(185,'[Cherry Berry Pie Crush] Anya',1177,39,14.293,5133.83,5360.16,1,'0000-00-00 00:00:00',NULL),(186,'[Snowmobile Races] Chilly',1169,39,155.763,5232.15,5168.86,1,'0000-00-00 00:00:00',NULL),(187,'[Lunaris Bear Throw] Mighail',1165,39,12.55,5220.76,5196.75,1,'0000-00-00 00:00:00',NULL),(188,'[Penguin Races] Penny',1166,39,254.3,5276.52,5276.63,1,'0000-00-00 00:00:00',NULL),(189,'[Ice Fishing] Val',1174,39,144.47,5208.38,5258.55,1,'0000-00-00 00:00:00',NULL),(190,'[Lunaris RPS] Evanna',1168,39,340.035,5298.56,5206.56,1,'0000-00-00 00:00:00',NULL),(191,'[Snowplowing for Treasure] Magnus',1167,39,243.497,5303.99,5171.08,1,'0000-00-00 00:00:00',NULL),(192,'[Cherry Berry Pie Crush] Anya',1176,39,254.3,5381.49,5260.66,1,'0000-00-00 00:00:00',NULL),(193,'[Snow Valley Biathlon] Viktoria',1170,39,243.497,5393.62,5044.77,1,'0000-00-00 00:00:00',NULL),(194,'[Cherry Berry Pie Crush] Anya',1178,39,197.989,5305.58,5095.4,1,'0000-00-00 00:00:00',NULL),(195,'[Event Info] Lucille Fete',1203,40,333.435,5254.64,5216.39,1,'0000-00-00 00:00:00',NULL),(196,'[Arua\'s Servant] Serenity',1078,41,19.965,5014.03,5298.4,1,'0000-00-00 00:00:00',NULL),(197,'[Archaeologist] Tolken',1980,41,345.707,5095.1,5299.21,1,'0000-00-00 00:00:00',NULL),(198,'[Orlean Ambassador] Bond',1077,41,180.5,5071.9,5258.5,1,'0000-00-00 00:00:00',NULL),(199,'[Hebarn\'s Vassal] Starburner',1079,41,148.307,5010.3,5194.24,1,'0000-00-00 00:00:00',NULL),(200,'[Archaeologist] Jorgus',1076,41,0,5104.47,5201.29,1,'0000-00-00 00:00:00',NULL),(201,'[Interplanetary Guide] Illiya',1188,51,148.307,5107.24,4996.57,1,'0000-00-00 00:00:00',NULL),(202,'[Tavern Owner] Anzhelika',1186,51,139.107,5263.61,5079.49,1,'0000-00-00 00:00:00',NULL),(203,'[Ferrell Guild Banker] Andre',1180,51,0,5414.86,5114.41,1,'0000-00-00 00:00:00',NULL),(204,'[Arumic Merchant] Pabel',1185,51,3.008,5298.98,5105.03,1,'0000-00-00 00:00:00',NULL),(205,'[Arumic Prophet] Olleck Basilasi',1173,51,178.499,5359.62,5085.72,1,'0000-00-00 00:00:00',NULL),(206,'[Monster Coin Trader] Dedev',1494,51,22.043,5350.81,5000.7,1,'0000-00-00 00:00:00',NULL),(207,'[Battlemaster] Sappho',1993,51,182.002,5429.85,5057.87,1,'0000-00-00 00:00:00',NULL),(208,'[Akram Ambassador] Eliot',1172,51,157.957,5285.69,4940.95,1,'0000-00-00 00:00:00',NULL),(209,'[Eucar Judge] Ishtal',1171,51,206.565,5430.09,4937.96,1,'0000-00-00 00:00:00',NULL),(210,'[Smith] Pavrick',1181,51,335.763,5440.45,5103.41,1,'0000-00-00 00:00:00',NULL),(211,'[Ferrell Guild Staff] Sergei',1184,51,258.129,5450.88,5077.06,1,'0000-00-00 00:00:00',NULL),(212,'[Shamanist Ambassador] Khiya',1196,51,198.637,5447.2,4948.15,1,'0000-00-00 00:00:00',NULL),(213,'[Gentleman Explorer] Toffee Coffington',1182,51,212.618,5655.85,5210.83,1,'0000-00-00 00:00:00',NULL),(214,'[Lost Lunaris Girl] Kiroro',1190,52,333.435,5168.52,4787.17,1,'0000-00-00 00:00:00',NULL),(215,'[Gentleman Explorer] Nicholai Coffington',1187,52,338.662,6145.36,5238.13,1,'0000-00-00 00:00:00',NULL),(216,'[Gentleman Explorer] Wilhelm Coffington',1183,53,0,6004.57,5368.4,1,'0000-00-00 00:00:00',NULL),(217,'[Shamanist] Est',1191,54,84.016,5088.26,4257.84,1,'0000-00-00 00:00:00',NULL),(218,'[Livestock Farmer] Kapeka',1194,55,61.813,5271.06,4623.53,1,'0000-00-00 00:00:00',NULL),(219,'[Akram Prince] Loderic',1189,58,93.995,5524.25,5008.43,1,'0000-00-00 00:00:00',NULL),(220,'[Interplanetary Guide] Chacha',1219,61,12.55,5226.6,4452.72,1,'0000-00-00 00:00:00',NULL),(221,'[Raknu Warrior] Toanu',1229,61,180,5434.62,4671.5,1,'0000-00-00 00:00:00',NULL),(222,'[Raknu Warrior] Guanu',1230,61,181.501,5424.42,4671.62,1,'0000-00-00 00:00:00',NULL),(223,'[Junon Order Elder] Oscar Patrick',1214,61,157.957,5371.18,4546.51,1,'0000-00-00 00:00:00',NULL),(224,'[Righteous Crusader] Harold Evan',1211,61,14.293,5369.75,4575.29,1,'0000-00-00 00:00:00',NULL),(225,'[Smith] Nel Eldora',1223,61,14.293,5395.55,4614.05,1,'0000-00-00 00:00:00',NULL),(226,'[Akram Ambassador] Jacklyn Cooper',1215,61,180,5430,4483.79,1,'0000-00-00 00:00:00',NULL),(227,'[Patrol Dog] Stephen',1244,61,180,5427.52,4482.43,1,'0000-00-00 00:00:00',NULL),(228,'[Raknu Warrior] Jeffrey Lloyd',1221,61,180,5434.01,4595.87,1,'0000-00-00 00:00:00',NULL),(229,'[Monster Coin Trader] Dyv\'d',1495,61,39.765,5382.18,4611.8,1,'0000-00-00 00:00:00',NULL),(230,'[Arumic Researcher] Catherine Clara',1212,61,330.956,5473.37,4618.79,1,'0000-00-00 00:00:00',NULL),(231,'[Ferrell Guild Staff] Gilbert',1213,61,217.594,5474.35,4513.74,1,'0000-00-00 00:00:00',NULL),(232,'[Storage Keeper] Dustin Leta',1222,61,197.989,5489.63,4553.05,1,'0000-00-00 00:00:00',NULL),(233,'[Patrol Dog] Max',1243,61,199.965,5487.27,4550.96,1,'0000-00-00 00:00:00',NULL),(234,'[Raknu Resident] Netty',1237,61,328.307,5494.83,4593.46,1,'0000-00-00 00:00:00',NULL),(235,'[Battlemaster] Rubee',1991,61,345.707,5610.94,4480.01,1,'0000-00-00 00:00:00',NULL),(236,'[Chef] Peppie',1224,61,190.865,5626.88,4435.97,1,'0000-00-00 00:00:00',NULL),(237,'[Raknu Chief] Darka Khan',1220,61,352.369,5650.09,4464.73,1,'0000-00-00 00:00:00',NULL),(238,'[Raknu Resident] Jerrita',1236,61,303.017,5657.17,4455.98,1,'0000-00-00 00:00:00',NULL),(239,'[Sikuku Warrior] Kilie',1257,62,333.435,5647.96,5126.88,1,'0000-00-00 00:00:00',NULL),(240,'[Sikuku Resident] Martie',1258,62,16.102,5638.21,5127.42,1,'0000-00-00 00:00:00',NULL),(241,'[Sikuku Resident] Carl',1259,62,95.984,5714.45,5121.83,1,'0000-00-00 00:00:00',NULL),(242,'[Sikuku Resident] Parah',1260,62,192.55,5721.78,5118.28,1,'0000-00-00 00:00:00',NULL),(243,'[Sikuku Chief] Namiel Char',1252,62,220.893,5896.76,5129.8,1,'0000-00-00 00:00:00',NULL),(244,'[Cleric] Jude',1251,62,330.956,5851.77,5209.4,1,'0000-00-00 00:00:00',NULL),(245,'[Bird] Hawker',1253,62,220.893,5901.42,5130.73,1,'0000-00-00 00:00:00',NULL),(246,'[Sikuku Warrior] Seka',1256,62,181.501,5817.34,5174,1,'0000-00-00 00:00:00',NULL),(247,'[Sikuku Resident] Shilma',1261,62,330.956,5854.29,5225.03,1,'0000-00-00 00:00:00',NULL),(248,'[Sikuku Warrior] Wounded Soldier',1262,62,206.565,5864.97,5211.14,1,'0000-00-00 00:00:00',NULL),(249,'[Sikuku Warrior] Wounded Soldier',1263,62,350.772,5862.55,5218.48,1,'0000-00-00 00:00:00',NULL),(250,'[Critically Wounded] Nukie',1266,62,202.043,5858.96,5208.03,1,'0000-00-00 00:00:00',NULL),(251,'[Sikuku Warrior] Yak',1255,62,24.237,5789.36,5051.35,1,'0000-00-00 00:00:00',NULL),(252,'[Sikuku Warrior] Ruduck',1254,62,22.043,5779.49,5040.41,1,'0000-00-00 00:00:00',NULL),(253,'[Archaeologist] Kentucky Jones',1028,65,14.293,5478.48,5413.51,1,'0000-00-00 00:00:00',NULL),(254,'[Herbalist] Helianthus',1029,65,155.002,5478.76,5364.14,1,'0000-00-00 00:00:00',NULL),(255,'[Smith] Kojo',1247,66,352.369,5545.17,5190.61,1,'0000-00-00 00:00:00',NULL),(256,'[Arumic Researcher] Redford',1265,66,44.459,5587.14,4807.23,1,'0000-00-00 00:00:00',NULL),(257,'[Cleric] Maria',1248,66,135.541,5668.65,4861.56,1,'0000-00-00 00:00:00',NULL),(258,'[Mountain Guide] Bennett ',1250,66,248.756,5687.64,4862.35,1,'0000-00-00 00:00:00',NULL),(259,'[Sikuku Tracker] Akuku',1245,66,317.949,5681.93,4879.12,1,'0000-00-00 00:00:00',NULL),(260,'[Ruins Curator] Hope',1249,66,180,5900.05,4341.24,1,'0000-00-00 00:00:00',NULL),(261,'[Ministry of Agriculture] Aquila',2150,71,52.485,5042.32,5304.58,1,'0000-00-00 00:00:00',NULL),(262,'[Pharaoh Queen] Alana',2121,71,0,5199.98,5399.09,1,'0000-00-00 00:00:00',NULL),(263,'[Royal Vizier] Kaltet XIV',2139,71,34.535,5168.18,5371.16,1,'0000-00-00 00:00:00',NULL),(264,'[Royal Vizier] Satet XIV',2140,71,325.465,5225.4,5371.71,1,'0000-00-00 00:00:00',NULL),(265,'[Orlean Ambassador] Solara',2141,71,60.166,5170.97,5309.67,1,'0000-00-00 00:00:00',NULL),(266,'[Manager of Ferrell] Ishmat',2126,71,0,5231.05,5285.76,1,'0000-00-00 00:00:00',NULL),(267,'[Weapon Merchant] Huzam',2122,71,0,5129.25,5289.15,1,'0000-00-00 00:00:00',NULL),(268,'[Armor Merchant] Azim',2124,71,0,5156.39,5289.25,1,'0000-00-00 00:00:00',NULL),(269,'[Mechanic] Kreinto',2128,71,352.369,5272.06,5290.73,1,'0000-00-00 00:00:00',NULL),(270,'[Hebarn\'s Vassal] Tama',2157,71,226.997,5218.91,5310.57,1,'0000-00-00 00:00:00',NULL),(271,'[Hebarn\'s Vassal] Nysaa',2158,71,320.235,5213.26,5349.05,1,'0000-00-00 00:00:00',NULL),(272,'[Hebarn\'s Vassal] Rexanne',2159,71,31.693,5210.63,5349.96,1,'0000-00-00 00:00:00',NULL),(273,'[Hebarn\'s Vassal] Nonus',2160,71,29.044,5215.06,5347.3,1,'0000-00-00 00:00:00',NULL),(274,'[Hebarn\'s Vassal] Onur',2162,71,166.295,5210.94,5346.89,1,'0000-00-00 00:00:00',NULL),(275,'[Arua\'s Servant] Meryem',2166,71,148.307,5186.99,5347.98,1,'0000-00-00 00:00:00',NULL),(276,'[Arua\'s Servant] Miho',2168,71,195.491,5191.03,5347.32,1,'0000-00-00 00:00:00',NULL),(277,'[Arua\'s Servant] Goran',2169,71,346.295,5189.61,5351.27,1,'0000-00-00 00:00:00',NULL),(278,'[Arua\'s Servant] Lugus',2170,71,133.003,5200.29,5317.75,1,'0000-00-00 00:00:00',NULL),(279,'[Arua\'s Servant] Ohad',2171,71,200.645,5186.24,5350.61,1,'0000-00-00 00:00:00',NULL),(280,'[Hebarn\'s Vassal] Uros',2161,71,0,5201.08,5302.63,1,'0000-00-00 00:00:00',NULL),(281,'[Arua\'s Servant] Henda',2167,71,17.989,5190.57,5327,1,'0000-00-00 00:00:00',NULL),(282,'[Invention] CG-3',2120,71,344.509,5279.4,5284.65,1,'0000-00-00 00:00:00',NULL),(283,'[Junon Order Elder] Hameed',2133,71,90,5186.94,5155.04,1,'0000-00-00 00:00:00',NULL),(284,'[Ferrell Guild Staff] Kareem',2136,71,88,5181.34,5132.4,1,'0000-00-00 00:00:00',NULL),(285,'[Event Guide] Jalilla',2137,71,194.293,5190.86,5259.98,1,'0000-00-00 00:00:00',NULL),(286,'[Lojala Blackbear Leader] Tondro',2138,71,293.031,5270.76,5187.31,1,'0000-00-00 00:00:00',NULL),(287,'[Smith] Mahyr',2127,71,275.984,5268.82,5142.06,1,'0000-00-00 00:00:00',NULL),(288,'[Oro Bar Owner] Sahrazod',2125,71,76.203,5235.65,5162.88,1,'0000-00-00 00:00:00',NULL),(289,'[Monster Coin Trader] Daih\'vyd',1496,71,0,5209,5209.9,1,'0000-00-00 00:00:00',NULL),(290,'[Valor Quest Giver] Roen',1497,71,342.011,5215.88,5139.86,1,'0000-00-00 00:00:00',NULL),(291,'[Battlemaster] Amber',1992,71,93.995,5127.36,5192.5,1,'0000-00-00 00:00:00',NULL),(292,'[Mechanic\'s Dog] Chopper',2151,71,351.841,5273.3,5272.75,1,'0000-00-00 00:00:00',NULL),(293,'[Ikaness Staff] Galiya',2130,71,264.016,5278.29,5109.64,1,'0000-00-00 00:00:00',NULL),(294,'[Righteous Crusader] Rahnan',2135,71,131.684,5181.24,5104.91,1,'0000-00-00 00:00:00',NULL),(295,'[Arumic Researcher] Fahimah',2134,71,82.038,5183.66,5086.18,1,'0000-00-00 00:00:00',NULL),(296,'[Gypsy Merchant] Nadeem',2131,71,1.501,5229.51,5078.33,1,'0000-00-00 00:00:00',NULL),(297,'[Livestock Farmer] Hafis',2132,71,330.956,5150.91,5042.62,1,'0000-00-00 00:00:00',NULL),(298,'[Orlean Ambassador] Lynd',2142,71,0,5166.8,5069.03,1,'0000-00-00 00:00:00',NULL),(299,'[Gunsmith] Rahd',2123,71,252.423,5203.26,5051.36,1,'0000-00-00 00:00:00',NULL),(300,'[Stargazer] Nubo',2129,71,351.841,5299.85,5434.61,1,'0000-00-00 00:00:00',NULL),(301,'[Interplanetary Guide] Nova',2101,73,0,5241.3,5225.49,1,'0000-00-00 00:00:00',NULL),(302,'[Wandering Rifter] Nimbo',2102,73,0,5328.58,5233.29,1,'0000-00-00 00:00:00',NULL),(303,'[Lojala Blackbear Captain] Skualo',2111,80,162.011,5000.09,5142.27,1,'0000-00-00 00:00:00',NULL),(304,'[Lojala Trader] Brizo',2112,80,44.459,5059.07,5160.72,1,'0000-00-00 00:00:00',NULL),(305,'[Lojala Storage Keeper] Ventego',2113,80,6.067,4985.28,5205.54,1,'0000-00-00 00:00:00',NULL),(306,'[Lojala Smith] Ekblovo',2114,80,52.486,5040.65,5211.01,1,'0000-00-00 00:00:00',NULL),(307,'[Fidelulo Blackbear Leader] Fulmo',2115,81,163.81,5389.23,5255.3,1,'0000-00-00 00:00:00',NULL),(308,'[Astrophysicist] Dr. Ega',2106,83,56.051,5086.34,5142.83,1,'0000-00-00 00:00:00',NULL),(309,'[Deckhand] Skully',2105,83,154.034,5052.14,5116.92,1,'0000-00-00 00:00:00',NULL),(310,'[First Mate] Bighand Jack',2104,83,136.342,5081.89,5112.39,1,'0000-00-00 00:00:00',NULL),(311,'[Dread Captain] Bruise',2103,83,167.945,5101.89,5091.76,1,'0000-00-00 00:00:00',NULL),(312,'[Desert Explorer] Bryll',2100,85,328.074,5529.65,5223,1,'0000-00-00 00:00:00',NULL);
/*!40000 ALTER TABLE `npcs` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `planets`
--

DROP TABLE IF EXISTS `planets`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `planets` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `name` varchar(100) NOT NULL,
  `image` varchar(100) NOT NULL,
  `created_at` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP,
  `updated_at` timestamp NULL DEFAULT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=5 DEFAULT CHARSET=latin1;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `planets`
--

LOCK TABLES `planets` WRITE;
/*!40000 ALTER TABLE `planets` DISABLE KEYS */;
INSERT INTO `planets` VALUES (1,'Junon','junon_planet.png','0000-00-00 00:00:00',NULL),(2,'Luna','luna_planet.png','0000-00-00 00:00:00',NULL),(3,'Eldeon','eldeon_planet.png','0000-00-00 00:00:00',NULL),(4,'Orlo','orlo_planet.png','0000-00-00 00:00:00',NULL);
/*!40000 ALTER TABLE `planets` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `users`
--

DROP TABLE IF EXISTS `users`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `users` (
  `id` int(10) unsigned NOT NULL AUTO_INCREMENT,
  `name` varchar(255) COLLATE utf8mb4_unicode_ci NOT NULL,
  `email` varchar(255) COLLATE utf8mb4_unicode_ci NOT NULL,
  `password` varchar(255) COLLATE utf8mb4_unicode_ci NOT NULL,
  `remember_token` varchar(100) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `users`
--

LOCK TABLES `users` WRITE;
/*!40000 ALTER TABLE `users` DISABLE KEYS */;
/*!40000 ALTER TABLE `users` ENABLE KEYS */;
UNLOCK TABLES;
/*!40103 SET TIME_ZONE=@OLD_TIME_ZONE */;

/*!40101 SET SQL_MODE=@OLD_SQL_MODE */;
/*!40014 SET FOREIGN_KEY_CHECKS=@OLD_FOREIGN_KEY_CHECKS */;
/*!40014 SET UNIQUE_CHECKS=@OLD_UNIQUE_CHECKS */;
/*!40101 SET CHARACTER_SET_CLIENT=@OLD_CHARACTER_SET_CLIENT */;
/*!40101 SET CHARACTER_SET_RESULTS=@OLD_CHARACTER_SET_RESULTS */;
/*!40101 SET COLLATION_CONNECTION=@OLD_COLLATION_CONNECTION */;
/*!40111 SET SQL_NOTES=@OLD_SQL_NOTES */;

-- Dump completed on 2017-09-21 19:32:22
