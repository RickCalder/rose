<?php

/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
*/

Route::get('/', "HomeController@show");
Route::get('/npcs', "NpcController@show");

// AJAX routes
Route::post('get-npc', 'NpcController@getNpc');
Route::post('get-npcs', 'NpcController@getNpcs');
Route::post('get-maps', 'MapController@getMaps');
Route::post('get-map', 'MapController@getMap');
