<?php

namespace App\Http\Controllers;
use App\Npc;
use App\Planet;
use App\Map;
use App\Event;

use App\Http\Controllers\Controller;
use Illuminate\Http\Request;

class HomeController extends Controller
{
  /**
   * Show the profile for the given user.
   *
   * @param  int  $id
   * @return Response
   */
  public function show() {
    $data['title'] = 'Home :: Rose Online Guide';
    $data['events'] = Event::all();

    return view('welcome', $data);
  }

}