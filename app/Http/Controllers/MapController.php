<?php

namespace App\Http\Controllers;
use App\Npc;
use App\Planet;
use App\Map;

use App\Http\Controllers\Controller;
use Illuminate\Http\Request;

class MapController extends Controller
{
  /**
   * Show the profile for the given user.
   *
   * @param  int  $id
   * @return Response
   */
  public function getMaps(Request $request) {
    $id = $request->all();
    $maps = Map::where('planet_id', $id['id'])->get()->toArray();
    print json_encode($maps);
  }

  public function getMap(Request $request) {
    $id = $request->all();
    $map = Map::where('id', $id['id'])->get()->toArray();
    print json_encode($map);
  }
}