<?php

namespace App\Http\Controllers;
use App\Npc;
use App\Planet;
use App\Map;

use App\Http\Controllers\Controller;
use Illuminate\Http\Request;

class NpcController extends Controller
{
  /**
   * Show the profile for the given user.
   *
   * @param  int  $id
   * @return Response
   */
  public function show() {
    $data['title'] = 'NPCs :: Rose Online Guide';
    $data['planets'] = Planet::all();
    $data['maps'] = Map::where('planet_id', 1)->get();
    $data['npcs'] = Npc::where('map_id', 1)->where('display', 1)->get();

    return view('npcs', $data);
  }

  public function getNpc(Request $request) {
    $npc_id = $request->all();
    $npc_data = Npc::where('id', $npc_id['npc_id'])->where('display', 1)->get()->toArray();
    print json_encode($npc_data);
  }

  public function getNpcs(Request $request) {
    $map_id = $request->all();
    $npcs = Npc::where('map_id', $map_id['map_id'])->where('display', 1)->get()->toArray();
    print json_encode($npcs);
  }
}